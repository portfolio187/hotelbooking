package com.company;





import java.util.*;


public class Hotelmethods {
    private List<Room> roomList;
    private List<HotelService> serviceList;

    static Scanner scanner = new Scanner(System.in);

    public Hotelmethods(List<Room> roomList, List<HotelService> serviceList) {
        this.roomList = roomList;
        this.serviceList = serviceList;
    }

    private List<HotelService> addserviceRoom1 = new ArrayList<>();
    private List<HotelService> addserviceRoom2 = new ArrayList<>();
    private List<HotelService> addserviceRoom3 = new ArrayList<>();
    private List<HotelService> addserviceRoom4 = new ArrayList<>();
    private List<HotelService> addserviceRoom5 = new ArrayList<>();

    int totalCost = 0;
    int serviceCost = 0;

    public void booking() {

        System.out.println("Vänligen välj ett rum att boka");

        for (Room s : roomList) {
            if (s.isRoomavailable()) {
                System.out.println(s.getId() + ":" + s.getRoomname() + " kostar " + s.getRoomcost());
            }
        }
        int choice;
        choice = scanner.nextInt() - 1;
        System.out.println("Du har bokat " + roomList.get(choice).getRoomname());
        roomList.get(choice).setRoomavailable(false);
    }

    public void CheckOut() {
        System.out.println("Vänligen välj ett rum att checka ut ifrån ");
        for (Room s : roomList) {
            if (!s.isRoomavailable()) {
                System.out.println(s.getId() + ". " + s.getRoomname() + " är bokat");
            }
        }
        int choice;
        choice = scanner.nextInt() - 1;
        System.out.println(roomList.get(choice).getRoomname() + " är nu tillgängligt ");
        switch (choice +1)  {
            case 1 -> addserviceRoom1.clear();
            case 2 -> addserviceRoom2.clear();
            case 3 -> addserviceRoom3.clear();
            case 4 -> addserviceRoom4.clear();
            case 5 -> addserviceRoom5.clear();
        }
        roomList.get(choice).setRoomavailable(true);
    }

    public void SeeAllRooms() {
        System.out.println("Bokade rum:");
        System.out.println();
        for (Room s : roomList) {
            if (!s.isRoomavailable()) {
                System.out.println(s.getId() + ". " + s.getRoomname() + " costs " + s.getRoomcost());
            }
        }
        System.out.println("----------------");
        System.out.println("Tillgängliga rum:");
        System.out.println();
        for (Room s : roomList) {
            if (s.isRoomavailable()) {
                System.out.println(s.getRoomname() + " kostar " + s.getRoomcost());
            }
        }
    }

    public static void startsida() {
        System.out.println("<--------Vänligen gör ett val-------> ");
        System.out.println("1. Boka ett tillgängligt rum");
        System.out.println("2. Checka ut från ett rum");
        System.out.println("3. Se alla rum");
        System.out.println("4. Lägga till roomservice på något rum");
        System.out.println("5. Ta bort roomservice från något rum");
        System.out.println("6. Se notan för alla rum");
        System.out.println("7. Avsluta");
        System.out.println();
    }

    public void AddService() {
        System.out.println("Till vilket rum vill du addera? ");
        for (Room s : roomList) {
            if (!s.isRoomavailable()) {
                System.out.println(s.getId() + ". " + s.getRoomname());
            }
        }
        int choice;
        choice = scanner.nextInt() - 1;
        System.out.println("Du valde rummet " + roomList.get(choice).getRoomname());
        System.out.println();
        switch (choice) {
            case 0 -> AddCost(addserviceRoom1);
            case 1 -> AddCost(addserviceRoom2);
            case 2 -> AddCost(addserviceRoom3);
            case 3 -> AddCost(addserviceRoom4);
            case 4 -> AddCost(addserviceRoom5);
        }
    }

    private void AddCost(List<HotelService> addServiceRoom) {
        System.out.println("Välj vad du vill addera? ");
        ServiceMenu();
        int choice = scanner.nextInt();
        switch (choice) {
            case 1 -> addServiceRoom.add(serviceList.get(0));
            case 2 -> addServiceRoom.add(serviceList.get(1));
            case 3 -> addServiceRoom.add(serviceList.get(2));
            case 4 -> addServiceRoom.add(serviceList.get(3));
            case 5 -> addServiceRoom.add(serviceList.get(4));
        }
        System.out.println("<-----------Klart----------> ");
        System.out.println();
    }

    public void DeleteService() {
        System.out.println("Till vilket rum vill du ta bort roomservice? ");
        for (Room s : roomList) {
            if (!s.isRoomavailable()) {
                System.out.println(s.getId() + ". " + s.getRoomname());
            }
        }
        int choice;
        choice = scanner.nextInt() - 1;
        System.out.println("Du valde rummet " + roomList.get(choice).getRoomname());
        System.out.println();
        switch (choice) {
            case 0 -> DeleteCost(addserviceRoom1);
            case 1 -> DeleteCost(addserviceRoom2);
            case 2 -> DeleteCost(addserviceRoom3);
            case 3 -> DeleteCost(addserviceRoom4);
            case 4 -> DeleteCost(addserviceRoom5);
        }
    }

    public void DeleteCost(List<HotelService> addServiceRoom) {
        System.out.println("Vad vill du ta bort? ");
        for (HotelService sir : addServiceRoom
        )
            System.out.println(sir.getId() +". " + sir.getName());
        int choice = scanner.nextInt();
        switch (choice) {
            case 1 -> addServiceRoom.remove(serviceList.get(0));
            case 2 -> addServiceRoom.remove(serviceList.get(1));
            case 3 -> addServiceRoom.remove(serviceList.get(2));
            case 4 -> addServiceRoom.remove(serviceList.get(3));
            case 5 -> addServiceRoom.remove(serviceList.get(4));
        }
        System.out.println("<-----------Klart----------> ");
        System.out.println();
    }

    public void showRevenue(){
        if(!roomList.get(0).isRoomavailable()) {
            countRevenue(addserviceRoom1, roomList.get(0).getRoomname(), roomList.get(0).getRoomcost());

        }if(!roomList.get(1).isRoomavailable()){
            countRevenue(addserviceRoom2, roomList.get(1).getRoomname(), roomList.get(1).getRoomcost());

        }if(!roomList.get(2).isRoomavailable()){
            countRevenue(addserviceRoom3, roomList.get(2).getRoomname(), roomList.get(2).getRoomcost());

        }if(!roomList.get(3).isRoomavailable()){
            countRevenue(addserviceRoom4, roomList.get(3).getRoomname(), roomList.get(3).getRoomcost());

        }if(!roomList.get(4).isRoomavailable()){
            countRevenue(addserviceRoom5, roomList.get(4).getRoomname(), roomList.get(4).getRoomcost());

        }
        System.out.println("Det totala beloppet för alla rum är " + totalCost + " sek");
        totalCost = 0;
    }

     public void countRevenue(List<HotelService> addServiceRoom, String roomName , int roomCost) {

         System.out.println("Baspris: " + roomName +  roomCost + " sek");
         for (HotelService l : addServiceRoom) {
             serviceCost = serviceCost + l.getCost();
             System.out.println("Roomservice: " + l.getName() + " " + l.getCost() + " sek");
         }
         totalCost = totalCost + roomCost + serviceCost;
         System.out.println("Notan för " + roomName + " är " + (roomCost + serviceCost) + " sek");
         System.out.println();

         serviceCost = 0;
     }

    public static void ServiceMenu() {
        System.out.println("På menyn finns det följande val ");
        System.out.println("1. Frukost");
        System.out.println("2. Middag");
        System.out.println("3. Vinpaket");
        System.out.println("4. Spa inträde");
        System.out.println("5. Massage");
    }
}