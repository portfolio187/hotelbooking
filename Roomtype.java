package com.company;


import java.util.Arrays;
import java.util.List;


public class Roomtype {
    static List<HotelService> serviceList;
    static List<Room> roomList;

    public Roomtype(){

        Room room1 = new Room("Jupiter ", 3000, true, 1);
        Room room2 = new Room("Saturnus ", 2500, true, 2);
        Room room3 = new Room("Venus ", 1800, true, 3);
        Room room4 = new Room("Pluto ", 1300, true, 4);
        Room room5 = new Room("Moon ", 25000, true, 5);

        roomList = Arrays.asList(room1, room2, room3, room4, room5);

        HotelService service1 = new HotelService("Frukost", 500, 1);
        HotelService service2 = new HotelService("Middag", 250, 2);
        HotelService service3 = new HotelService("Vinpaket", 200, 3);
        HotelService service4 = new HotelService("Spa inträde", 700, 4);
        HotelService service5 = new HotelService("Massage", 500, 5);

        serviceList = Arrays.asList(service1, service2, service3, service4, service5);

    }

}
