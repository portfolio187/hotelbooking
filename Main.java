package com.company;


import java.util.*;

public class Main {
    static Scanner scanner = new Scanner(System.in);




    public static void main(String[] args) {

        Roomtype roomtype = new Roomtype();
        Hotelmethods hotelMethods = new Hotelmethods(Roomtype.roomList, Roomtype.serviceList);

        boolean menyRullar = true;
        while (menyRullar) {
            Hotelmethods.startsida();
            int menyval = scanner.nextInt();
            switch (menyval) {
                case 1 -> hotelMethods.booking();
                case 2 -> hotelMethods.CheckOut();
                case 3 -> hotelMethods.SeeAllRooms();
                case 4 -> hotelMethods.AddService();
                case 5 -> hotelMethods.DeleteService();
                case 6 -> hotelMethods.showRevenue();
                case 7 -> menyRullar = false;
            }
        }
    }
}





