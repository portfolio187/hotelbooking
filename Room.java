package com.company;

public class Room {

    private String roomname;
    private int roomcost;
    private boolean roomavailable;
    private int id;

    public Room(String roomname, int roomcost, boolean roomavailable, int id) {
        this.roomname = roomname;
        this.roomcost = roomcost;
        this.roomavailable = roomavailable;
        this.id = id;
    }


    public String getRoomname() {

        return roomname;
    }

    public void setRoomname(String roomname) {

        this.roomname = roomname;
    }

    public int getRoomcost() {

        return roomcost;
    }

    public void setRoomcost(int roomcost) {

        this.roomcost = roomcost;
    }

    public boolean isRoomavailable() {

        return roomavailable;
    }

    public void setRoomavailable(boolean roomavailable) {
        this.roomavailable = roomavailable;

    }

    public int getId() {
        return id;
    }
}



