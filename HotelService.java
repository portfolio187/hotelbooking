package com.company;

public class HotelService {

    private String name;
    private int cost;
    private int id;



    public HotelService(String name, int cost, int id) {
        this.name = name;
        this.cost = cost;
        this.id = id;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
